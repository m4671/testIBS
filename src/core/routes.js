import VueRouter from "vue-router";
import LoginView from "@/features/login/Login_view";
import ResetPasswordView from "@/features/login/reset_password_view";
import LayoutView from "@/components/layout/layout";
import ProfileView from "@/features/profile/profile_view";
import ChangePasswordView from "@/features/profile/changepassword_view";
import UserManagementListView from "@/features/usermanager/usermanagement/usermanagement_list";
import UserManagementAddView from "@/features/usermanager/usermanagement/usermanagement_add";
import UserManagementEditView from "@/features/usermanager/usermanagement/usermanagement_edit";
import PlacingView from "@/features/broking/placing/placing_view";
import PlacingView2 from "@/features/broking/placing/placing_view2";
import PlacingAdd from "@/features/broking/placing/placing_add";
import PlacingList from "@/features/broking/placing/placing_list";
import PlacingListV2 from "@/features/broking/placing/placing_list_v2";
import ProvinceListView from "@/features/configuration/areamaster/province/province_list";
import ProvinceAddView from "@/features/configuration/areamaster/province/province_add";
import ProvinceEditView from "@/features/configuration/areamaster/province/province_edit";

import RegencyListView from "@/features/configuration/areamaster/regency/regency_list";
import RegencyAddView from "@/features/configuration/areamaster/regency/regency_add";
import RegencyEditView from "@/features/configuration/areamaster/regency/regency_edit";
import RegencyDetailView from "@/features/configuration/areamaster/regency/regency_detail";
import DistrictListView from "@/features/configuration/areamaster/district/district_list";
import VillageListView from "@/features/configuration/areamaster/village/village_list";
import VillageAddView from "@/features/configuration/areamaster/village/village_add";
import VillageEditView from "@/features/configuration/areamaster/village/village_edit";
import VillageDetailView from "@/features/configuration/areamaster/village/village_detail";
import CurrencyManager from "@/features/configuration/currency/currency_manager";
import CurrencyAddView from "@/features/configuration/currency/currency/currency_add";
import CurrencyDetailView from "@/features/configuration/currency/currency/currency_view";
import CurrencyEditView from "@/features/configuration/currency/currency/currency_edit";
import ExchangeRateAddView from "@/features/configuration/currency/exchangerate/exchangerate_add";
import ExchangeRateDetailView from "@/features/configuration/currency/exchangerate/exchangerate_view";
import ExchangeRateEditView from "@/features/configuration/currency/exchangerate/exchangerate_edit";
import AgentBrokerListView from "@/features/account/agentbroker/agent_broker_list";
import AgentBrokerAddView from "@/features/account/agentbroker/agent_broker_add";
import AgentBrokerEditView from "@/features/account/agentbroker/agent_broker_edit";
import AgentBrokerDetailView from "@/features/account/agentbroker/agent_broker_view";
import AdjusterMasterListView from "@/features/account/adjuster/adjuster_list";
import AdjusterMasterAddView from "@/features/account/adjuster/adjuster_add";
import AdjusterMasterEditView from "@/features/account/adjuster/adjuster_edit";
import AdjusterMasterDetailView from "@/features/account/adjuster/adjuster_view";
import RoleManagementListView from "@/features/usermanager/rolemanagement/role_management_list";
import RoleManagementAddView from "@/features/usermanager/rolemanagement/role_management_add";
import RoleManagementEditView from "@/features/usermanager/rolemanagement/role_management_edit";
import ParameterConfiguration from "@/features/configuration/parameterconfiguration/paramConfiguration_list";
import DistrictAddView from "@/features/configuration/areamaster/district/district_add";
import DistrictEditView from "@/features/configuration/areamaster/district/district_edit";
import ParameterConfigurationAdd from "@/features/configuration/parameterconfiguration/paramConfiguration_add";
import ParameterConfigurationDetail from "@/features/configuration/parameterconfiguration/paramConfiguration_detail";
import ParameterConfigurationEdit from "@/features/configuration/parameterconfiguration/paramConfiguration_edit";

import TemplateConfigurationListView from "@/features/configuration/templateconfiguration/template_configuration_list";
import TemplateConfigurationItemAdd from "@/features/configuration/templateconfiguration/templateitem/template_item_add";
import TemplateConfigurationItemEdit from "@/features/configuration/templateconfiguration/templateitem/template_item_edit";

import TemplateConfigurationConfigAdd from "@/features/configuration/templateconfiguration/templateconfig/template_config_add";
import TemplateConfigurationConfigEdit from "@/features/configuration/templateconfiguration/templateconfig/template_config_edit";
import BankMasterList from "@/features/configuration/bankMaster/bankMaster_list";
import BankMasterAdd from "@/features/configuration/bankMaster/bankMaster_add";
import BankMasterEdit from "@/features/configuration/bankMaster/bankMaster_edit";
import CoaMasterList from "@/features/configuration/coaMaster/coaMaster_list";
import CoaMasterAdd from "@/features/configuration/coaMaster/coaMaster_add";
import CoaMasterEdit from "@/features/configuration/coaMaster/coaMaster_edit";
import ClassOfRiskList from "@/features/configuration/classofrisk/classOfRisk_list";
import ClassOfRiskAdd from "@/features/configuration/classofrisk/classOfRisk_add";
import ClassOfRiskEdit from "@/features/configuration/classofrisk/classOfRisk_edit";
import CustomerOpportunity from "@/features/sales/customer/customerOpportunity_list";
import CustomerOpportunityAdd from "@/features/sales/customer/customerOpportunity_add";
import CustomerOpportunityEdit from "@/features/sales/customer/customerOpportunity_edit";
import sample1 from "@/components/samples/sample1";
import AccountManagement from "@/features/account/accountManagement/account_management_list";
import AccountManagementEdit from "@/features/account/accountManagement/account_management_edit";
import AccountManagementTest from "@/features/account/accountManagement/account_management_test";
import AccountManagementAdd from "@/features/account/accountManagement/account_management_add";
import AccountManagementView from "@/features/account/accountManagement/account_management_view";
import AccountManagementFinance from "@/features/account/accountManagement/account_management_finance_list";
import AccountManagementFinanceEdit from "@/features/account/accountManagement/account_management_finance_edit";
import ViewPolis from "@/features/account/accountManagement/view_polis";
import InsuranceMasterList from "@/features/account/insuranceMaster/insuranceMaster_list";
import InsuranceMasterAdd from "@/features/account/insuranceMaster/insuranceMaster_add";
import InsuranceMasterView from "@/features/account/insuranceMaster/insuranceMaster_view";
import CreateInvoiceList from "@/features/invoice/createInvoice/create_invoice_list";
import CreateInvoiceAdd from "@/features/invoice/createInvoice/invoiceAdd/create_invoice_add";
import CreateInvoiceBulk from "@/features/invoice/createInvoice/bulkInvoice/bulk_invoice_list";
import ViewInvoiceBulk from "@/features/invoice/createInvoice/bulkInvoice/bulk_invoice_view";
import InvoiceManagementList from "@/features/invoice/invoiceManagement/invoice_management_list";
import LapsedInvoiceList from "@/features/invoice/lapsedInvoice/lapsed_invoice_list";
import CreateClaimsListView from "@/features/claims/createclaims/create_claims_view";
import SingleInsuredList from "@/features/claims/createclaims/single/single_insured_view";
import MultiInsuredList from "@/features/claims/createclaims/multi/multi_insured_view";
import CreateSingleInsured from "@/features/claims/createclaims/create/createSingleInsured";
import CreateMultiInsured from "@/features/claims/createclaims/create/createMultiInsured";
import addStandardTransaction from "@/features/claims/standardTransaction/add_standardTransaction";
import editStandardTransaction from "@/features/claims/standardTransaction/view_standardTransaction";
import claimMaintenance from "@/features/claims/queryClaimsTrans/claimMaintenance_lis";
import viewMaintenance from "@/features/claims/queryClaimsTrans/claimMaintenance_view";
import ViewClaimsTrans from "@/features/claims/queryClaimsTrans/view_claim_trans";
import QuotationList from "@/features/broking/quotation/quotation_list";
import CoverNoteList from "@/features/broking/coverNote/coverNote_list";
import CreateInvoiceView from "../features/invoice/createInvoice/invoiceView/create_invoice_view.vue";
import CreateInvoiceEdit from "../features/invoice/createInvoice/invoiceEdit/create_invoice_view.vue";
import CreateInvoiceCopy from "../features/invoice/createInvoice/invoiceCopy/create_invoice_copy.vue";
import CreateInvoiceEndorsement from "../features/invoice/createInvoice/invoiceEndorsement/create_invoice_endorsement.vue";
import CreateInvoiceCancellation from "../features/invoice/createInvoice/invoiceCancellation/create_invoice_cancellation.vue";
import CreateInvoiceCommission from "../features/invoice/createInvoice/invoiceCommission/create_invoice_commission.vue";
import QuotationEditView from "@/features/broking/quotation/quotation_view";
import QuotationEditView2 from "@/features/broking/quotation/quotation_view2";
import QuotationCreate from "@/features/broking/quotation/quotation_create";

import CoverNoteCreate from "@/features/broking/coverNote/coverNote_Create";
import InvoiceManagementView from "../features/invoice/invoiceManagement/invoiceManagementView/invoice_management_view";
import InvoiceManagementEndor from "../features/invoice/invoiceManagement/invoiceManagementEndor/invoice_management_endor.vue";
import CoverNoteView from "@/features/broking/coverNote/coverNote_view";
import CoverNoteView2 from "@/features/broking/coverNote/coverNote_view2";
import EndorsementList from "@/features/broking/endorsement/endorsement_list";
import EndorsemenCreate from "@/features/broking/endorsement/endorsement_create";
import EndorsemenView from "@/features/broking/endorsement/endorsement_view";
import EndorsemenView2 from "@/features/broking/endorsement/endorsement_view2";
import listStandardTransaction from "@/features/claims/standardTransaction/list_standardTransaction";

import CashReceiptListView from "@/features/finance/cashreceipt/cash_receipt_list";
import CashReceiptAddView from "@/features/finance/cashreceipt/cash_receipt_add";
import CashReceiptEditView from "@/features/finance/cashreceipt/cash_receipt_edit";
import CashReceiptManualListView from "@/features/finance/cashreceiptmanual/cash_receipt_manual_list";
import CashReceiptManualAddView from "@/features/finance/cashreceiptmanual/cash_receipt_manual_add";
import CashReceiptManualEditView from "@/features/finance/cashreceiptmanual/cash_receipt_manual_edit";
import CreditApplicationListView from "@/features/finance/creditapplication/credit_application_list";
import CreditApplicationAddView from "@/features/finance/creditapplication/credit_application_add";
import CreditApplicationEditView from "@/features/finance/creditapplication/credit_application_edit";
import RefundListView from "@/features/finance/refund/refund_list";
import RefundAddView from "@/features/finance/refund/refund_add";
import RefundEditView from "@/features/finance/refund/refund_edit";
import QueryDebtorListView from "@/features/finance/querydebtor/query_debtor_list_v2";
import QueryCreditorListView from "@/features/finance/querycreditor/query_creditor_list";
import SoaManualListView from "@/features/finance/soamanual/soa_manual_list";
import SoaManualAddView from "@/features/finance/soamanual/soa_manual_add";
import SoaManualEditView from "@/features/finance/soamanual/soa_manual_edit";

import FinancialPaidDirectList from "@/features/finance/paidDirect/paidDirect_list";
import FinancialPaidDirectAdd from "@/features/finance/paidDirect/paidDirect_add";
import FinancialPaidDirectEdit from "@/features/finance/paidDirect/paidDirect_edit-v2";
import FinancialPaidDirectView from "@/features/finance/paidDirect/paidDirect_view";

import FinancialPremiumAllocationList from "@/features/finance/premiumAllocation/premiumAllocation_list";
import FinancialPremiumAllocationAdd from "@/features/finance/premiumAllocation/premiumAllocation_add";
import FinancialPremiumAllocationEdit from "@/features/finance/premiumAllocation/premiumAllocation_edit-v2";
import FinancialPremiumAllocationView from "@/features/finance/premiumAllocation/premiumAllocation_view";

import FinancialPaidDirectDebtorList from "@/features/financeclaim/paiddirectdebtor/paid_direct_list";
import FinancialPaidDirecDebtortAdd from "@/features/financeclaim/paiddirectdebtor/paid_direct_add";
import FinancialPaidDirectDebtorEdit from "@/features/financeclaim/paiddirectdebtor/paid_direct_edit";
import FinancialPaidDirectDebtorView from "@/features/financeclaim/paiddirectdebtor/paid_direct_view";

import FinancialRemittanceList from "@/features/finance/remittance/remittance_list";
import FinancialRemittanceAdd from "@/features/finance/remittance/remittance_add";
import FinancialRemittanceEdit from "@/features/finance/remittance/remittance_edit";
import FinancialRemittanceEditable from "@/features/finance/remittance/remittance_editable";

import FinancialPaymentCreditorList from "@/features/financeclaim/paymentcreditor/payment_list";
import FinancialPaymentCreditorAdd from "@/features/financeclaim/paymentcreditor/payment_add";
import FinancialPaymentCreditorView from "@/features/financeclaim/paymentcreditor/payment_view";
import FinancialPaymentCreditorEdit from "@/features/financeclaim/paymentcreditor/payment_edit";

import FinancialPaymentDebtorList from "@/features/financeclaim/paymentdebtor/payment_list";
import FinancialPaymentDebtorAdd from "@/features/financeclaim/paymentdebtor/payment_add";
import FinancialPaymentDebtorEdit from "@/features/financeclaim/paymentdebtor/payment_edit";

import JournalCurrencyList from "@/features/finance/journal/journalCurrency_list";
import JournalCurrencyAdd from "@/features/finance/journal/journalCurrency_add";
import JournalCurrencyView from "@/features/finance/journal/journalCurrency_view";

import AdjustmentList from "@/features/finance/adjustment/adjustment_list";
import AdjustmentAdd from "@/features/finance/adjustment/adjustment_add";
import AdjustmentEdit from "@/features/finance/adjustment/adjustment_edit";

import BrokingReportList from "@/features/report/broking/brokingReportList";
import FinanceReportList from "@/features/report/finance/report_finance_list";
import FinanceReportView from "@/features/report/finance/report_finance_view";
import FinanceClaimReportList from "@/features/report/financeclaim/report_finance_claim_list";
import ClaimReportList from "@/features/report/claim/report_claim_list";
import ITReportList from "@/features/report/it/report_it_list";
import AccountingReportList from "@/features/report/accounting/report_accounting_list";

import QueryDebtorClaimListView from "@/features/financeclaim/querydebtor/query_debtor_list_v2";
import QueryCreditorClaimListView from "@/features/financeclaim/querycreditor/query_creditor_list";
import CashReceiptClaimDebtorListView from "@/features/financeclaim/cashreceiptclaimdebtor/cash_receipt_claim_debtor_list";
import CashReceiptClaimDebtorAddView from "@/features/financeclaim/cashreceiptclaimdebtor/cash_receipt_claim_debtor_add";
import CashReceiptClaimDebtorEditView from "@/features/financeclaim/cashreceiptclaimdebtor/cash_receipt_claim_debtor_edit";
import CashReceiptClaimCreditorListView from "@/features/financeclaim/cashreceiptclaimcreditor/cash_receipt_claim_creditor_list";
import CashReceiptClaimCreditorAddView from "@/features/financeclaim/cashreceiptclaimcreditor/cash_receipt_claim_creditor_add";
import CashReceiptClaimCreditorEditView from "@/features/financeclaim/cashreceiptclaimcreditor/cash_receipt_claim_creditor_edit";
import FundingClaimDebtorListView from "@/features/financeclaim/fundingclaimdebtor/funding_claim_debtor_list";
import FundingClaimDebtorAddView from "@/features/financeclaim/fundingclaimdebtor/funding_claim_debtor_add";
import FundingClaimDebtorEditView from "@/features/financeclaim/fundingclaimdebtor/funding_claim_debtor_edit";
import CreditApplicationClaimDebtorListView from "@/features/financeclaim/creditapplicationclaimdebtor/credit_application_claim_debtor_list";
import CreditApplicationClaimDebtorAddView from "@/features/financeclaim/creditapplicationclaimdebtor/credit_application_claim_debtor_add";
import CreditApplicationClaimDebtorEditView from "@/features/financeclaim/creditapplicationclaimdebtor/credit_application_claim_debtor_edit";
import CreditApplicationClaimCreditorListView from "@/features/financeclaim/creditapplicationclaimcreditor/credit_application_claim_creditor_list";
import CreditApplicationClaimCreditorAddView from "@/features/financeclaim/creditapplicationclaimcreditor/credit_application_claim_creditor_add";
import CreditApplicationClaimCreditorEditView from "@/features/financeclaim/creditapplicationclaimcreditor/credit_application_claim_creditor_edit";
import AdjustmentDebtorListView from "@/features/financeclaim/adjustmentdebtor/adjustment_list";
import AdjustmentDebtorAddView from "@/features/financeclaim/adjustmentdebtor/adjustment_add";
import AdjustmentDebtorEditView from "@/features/financeclaim/adjustmentdebtor/adjustment_edit";
import AdjustmentCreditorListView from "@/features/financeclaim/adjustmentcreditor/adjustment_list";
import AdjustmentCreditorAddView from "@/features/financeclaim/adjustmentcreditor/adjustment_add";
import AdjustmentCreditorrEditView from "@/features/financeclaim/adjustmentcreditor/adjustment_edit";
import RefundDebtorListView from "@/features/financeclaim/refunddebtor/refund_list";
import RefundDebtorAddView from "@/features/financeclaim/refunddebtor/refund_add";
import RefundDebtorEditView from "@/features/financeclaim/refunddebtor/refund_edit";
import RefundCreditorListView from "@/features/financeclaim/refundcreditor/refund_list";
import RefundCreditorAddView from "@/features/financeclaim/refundcreditor/refund_add";
import RefundCreditorEditView from "@/features/financeclaim/refundcreditor/refund_edit";
import JournalDebtorListView from "@/features/financeclaim/journaldebtor/journalCurrency_list";
import JournalDebtorAddView from "@/features/financeclaim/journaldebtor/journalCurrency_add";
import JournalDebtorEditView from "@/features/financeclaim/journaldebtor/journalCurrency_view";
import JournalCreditorListView from "@/features/financeclaim/journalcreditor/journalCurrency_list";
import JournalCreditorAddView from "@/features/financeclaim/journalcreditor/journalCurrency_add";
import JournalCreditorEditView from "@/features/financeclaim/journalcreditor/journalCurrency_view";

import VatNumberListView from "@/features/tax/vatnumber/vat_number_list";
import VatNumberAddView from "@/features/tax/vatnumber/vat_number_add";
import VatNumberEditView from "@/features/tax/vatnumber/vat_number_edit";
import VatManualListView from "@/features/tax/vatmanual/vat_manual_list";
import VatManualAddView from "@/features/tax/vatmanual/vat_manual_add";
import VatManualEditView from "@/features/tax/vatmanual/vat_manual_edit";
import FakturPajakUnderwriterListView from "@/features/tax/fakturpajakunderwriter/faktur_pajak_underwriter_list";
import FakturPajakUnderwriterAddView from "@/features/tax/fakturpajakunderwriter/faktur_pajak_underwriter_add";
import FakturPajakUnderwriterEditView from "@/features/tax/fakturpajakunderwriter/faktur_pajak_underwriter_edit";
import FakturPajakLampiranView from "@/features/tax/fakturpajaklampiran/faktur_pajak_lampiran_view";
import GenerateVatListingListView from "@/features/tax/generatevatlisting/generate_vat_listing_list";
import GenerateVatListingView from "@/features/tax/generatevatlisting/generate_vat_listing_view";
import AuditControl from "@/features/operation/auditControl/audit_control";
import ClosingControl from "@/features/operation/closingControl/closing_control";
import Budgeting from "@/features/operation/budgeting/budgeting";
import BudgetingAddView from "@/features/operation/budgeting/budgeting_add";
import BudgetingEditView from "@/features/operation/budgeting/budgeting_view";
import EngineeringFeeListView from "@/features/finance/engineeringfee/engineering_fee_list";
import EngineeringFeeListEdit from "@/features/finance/engineeringfee/engineering_fee_edit";

import Vue from "vue";
import authHelper from "@/helper/authHelper";
import developmentHelper from "@/helper/developmentHelper";
import richTextEditor_Example from "@/components/samples/richTextEditor_Example";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: {
      name: "login",
    },
  },
  {
    path: "/login",
    name: "login",
    component: LoginView,
    // beforeEnter: ifNotAuthenticated
    meta: {
      requiresAuth: false,
      requiresPrivilege: false,
    },
  },
  {
    path: "/reset-password",
    name: "reset-password",
    component: ResetPasswordView,
    // beforeEnter: ifNotAuthenticated
    meta: {
      requiresAuth: false,
      requiresPrivilege: false,
    },
  },
  {
    path: "/home",
    name: "home",
    component: LayoutView,
    meta: {
      requiresAuth: true,
      requiresPrivilege: false,
    },
    children: [
      {
        path: "profile",
        name: "profile",
        // beforeEnter: ifAuthenticated,
        component: ProfileView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "change-password",
        name: "change-password",
        // beforeEnter: ifAuthenticated,
        component: ChangePasswordView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "usermanagement",
        name: "user-management-list",
        // beforeEnter: ifAuthenticated,
        component: UserManagementListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "usermanagement/add",
        name: "user-management-add",
        // beforeEnter: ifAuthenticated,
        component: UserManagementAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "usermanagement/edit",
        name: "user-management-edit",
        // beforeEnter: ifAuthenticated,
        component: UserManagementEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "sample",
        name: "sample-view",
        component: sample1,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "sample/richTextEditor",
        name: "richText-editor",
        component: richTextEditor_Example,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "placing/placing-view",
        name: "placing-edit",
        // beforeEnter: ifAuthenticated,
        component: PlacingView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "placing/placing-view2",
        name: "placing-edit2",
        // beforeEnter: ifAuthenticated,
        component: PlacingView2,
        // meta: {
        //   requiresAuth: true,
        //   requiresPrivilege: true,
        // },
      },
      {
        path: "placing/placing-add",
        name: "placing-add",
        // beforeEnter: ifAuthenticated,
        component: PlacingAdd,
      },
      {
        path: "placing",
        name: "placing-list",
        // beforeEnter: ifAuthenticated,
        component: PlacingList,
      },
      {
        path: "templateconfiguration",
        name: "template-configuration-list",
        // beforeEnter: ifAuthenticated,
        component: TemplateConfigurationListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "templateconfiguration/config-item-add",
        name: "template-config-item-add",
        // beforeEnter: ifAuthenticated,
        component: TemplateConfigurationItemAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "templateconfiguration/config-item-edit",
        name: "template-config-item-edit",
        // beforeEnter: ifAuthenticated,
        component: TemplateConfigurationItemEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },

      {
        path: "templateconfiguration/config-add",
        name: "template-config-config-add",
        // beforeEnter: ifAuthenticated,
        component: TemplateConfigurationConfigAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "templateconfiguration/config",
        name: "template-config-config-edit",
        // beforeEnter: ifAuthenticated,
        component: TemplateConfigurationConfigEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "province",
        name: "area-province-list",
        // beforeEnter: ifAuthenticated,
        component: ProvinceListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "province/province-add",
        name: "area-province-add",
        // beforeEnter: ifAuthenticated,
        component: ProvinceAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },

      {
        path: "province/province-edit",
        name: "area-province-edit",
        // beforeEnter: ifAuthenticated,
        component: ProvinceEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },

      {
        path: "regency",
        name: "area-regency-list",
        // beforeEnter: ifAuthenticated,
        component: RegencyListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "regency/regency-add",
        name: "area-regency-add",
        // beforeEnter: ifAuthenticated,
        component: RegencyAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "regency/regency-edit",
        name: "area-regency-edit",
        // beforeEnter: ifAuthenticated,
        component: RegencyEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "regency/regency-detail",
        name: "area-regency-detail",
        // beforeEnter: ifAuthenticated,
        component: RegencyDetailView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "district",
        name: "area-district-list",
        // beforeEnter: ifAuthenticated,
        component: DistrictListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "district/district-add",
        name: "area-district-add",
        // beforeEnter: ifAuthenticated,
        component: DistrictAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "district/district-edit",
        name: "area-district-edit",
        // beforeEnter: ifAuthenticated,
        component: DistrictEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "village",
        name: "area-village-list",
        // beforeEnter: ifAuthenticated,
        component: VillageListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "village/village-add",
        name: "area-village-add",
        // beforeEnter: ifAuthenticated,
        component: VillageAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "village/village-edit",
        name: "area-village-edit",
        // beforeEnter: ifAuthenticated,
        component: VillageEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "village/village-detail",
        name: "area-village-detail",
        // beforeEnter: ifAuthenticated,
        component: VillageDetailView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "parameterconfiguration",
        name: "parameter-configuration",
        component: ParameterConfiguration,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "parameterconfiguration/add",
        name: "parameter-configuration-add",
        component: ParameterConfigurationAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "parameterconfiguration/detail",
        name: "parameter-configuration-detail",
        component: ParameterConfigurationDetail,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "parameterconfiguration/edit",
        name: "parameter-configuration-edit",
        component: ParameterConfigurationEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "currency-manager",
        name: "currency-manager-list",
        // beforeEnter: ifAuthenticated,
        component: CurrencyManager,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "currency-manager/currency-add",
        name: "currency-add",
        component: CurrencyAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "currency-manager/currency-view",
        name: "currency-view",
        component: CurrencyDetailView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "currency-manager/currency-edit",
        name: "currency-edit",
        component: CurrencyEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "currency-manager/exchange-rate-add",
        name: "exchange-rate-add",
        component: ExchangeRateAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "currency-manager/exchange-rate-view",
        name: "exchange-rate-view",
        component: ExchangeRateDetailView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "currency-manager/exchange-rate-edit",
        name: "exchange-rate-edit",
        component: ExchangeRateEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "agent-broker",
        name: "agent-broker-list",
        component: AgentBrokerListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "agent-broker/add",
        name: "agent-broker-add",
        component: AgentBrokerAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "agent-broker/view",
        name: "agent-broker-view",
        component: AgentBrokerDetailView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "agent-broker/edit",
        name: "agent-broker-edit",
        component: AgentBrokerEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "adjuster",
        name: "adjuster-list",
        component: AdjusterMasterListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "adjuster/add",
        name: "adjuster-add",
        component: AdjusterMasterAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "adjuster/view",
        name: "adjuster-view",
        component: AdjusterMasterDetailView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "adjuster/edit",
        name: "adjuster-edit",
        component: AdjusterMasterEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "role-management",
        name: "role-management-list",
        component: RoleManagementListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "role-management/add",
        name: "role-management-add",
        component: RoleManagementAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "role-management/edit",
        name: "role-management-edit",
        component: RoleManagementEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "bankMaster",
        name: "mbank-list",
        component: BankMasterList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "bankMaster/bankMaster-add",
        name: "mbank-add",
        component: BankMasterAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "bankMaster/bankMaster-edit",
        name: "mbank-edit",
        component: BankMasterEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "coaMaster",
        name: "akuncoa-list",
        component: CoaMasterList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "coaMaster/coaMaster-add",
        name: "akuncoa-add",
        component: CoaMasterAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "coaMaster/coaMaster-edit",
        name: "akuncoa-edit",
        component: CoaMasterEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "classofrisk",
        name: "class-of-risk-list",
        component: ClassOfRiskList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "classofrisk/classofrisk-add",
        name: "class-of-risk-add",
        component: ClassOfRiskAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "classofrisk/classofrisk-edit",
        name: "class-of-risk-edit",
        component: ClassOfRiskEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "customerOpportunity",
        name: "customer-opportunity-list",
        // beforeEnter: ifAuthenticated,
        component: CustomerOpportunity,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "customerOpportunity/add",
        name: "customer-opportunity-add",
        // beforeEnter: ifAuthenticated,
        component: CustomerOpportunityAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "customerOpportunity/edit",
        name: "customer-opportunity-edit",
        // beforeEnter: ifAuthenticated,
        component: CustomerOpportunityEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "accountManagement",
        name: "account-management-list",
        // beforeEnter: ifAuthenticated,
        component: AccountManagement,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "accountManagement/accountManagement-edit",
        name: "account-management-edit",
        // beforeEnter: ifAuthenticated,
        component: AccountManagementEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "accountManagement/accountManagement-test",
        name: "account-management-test",
        // beforeEnter: ifAuthenticated,
        component: AccountManagementTest,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "accountManagement/accountManagement-add",
        name: "account-management-add",
        // beforeEnter: ifAuthenticated,
        component: AccountManagementAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "accountManagement/accountManagement-view",
        name: "account-management-view",
        // beforeEnter: ifAuthenticated,
        component: AccountManagementView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "accountManagement/view_polis",
        name: "view-Polis",
        // beforeEnter: ifAuthenticated,
        component: ViewPolis,
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "accountManagementFinance",
        name: "account-management-finance-list",
        // beforeEnter: ifAuthenticated,
        component: AccountManagementFinance,
        meta: {
          requiresAuth: false,
          requiresPrivilege: false,
        },
      },
      {
        path: "accountManagementFinance/accountManagementFinance-edit",
        name: "account-management-finance-edit",
        // beforeEnter: ifAuthenticated,
        component: AccountManagementFinanceEdit,
        meta: {
          requiresAuth: false,
          requiresPrivilege: false,
        },
      },
      {
        path: "insuranceMaster",
        name: "insurance-master-list",
        component: InsuranceMasterList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },

      {
        path: "insuranceMaster/insuranceMaster-add",
        name: "insurance-master-add",
        component: InsuranceMasterAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "insuranceMaster/insuranceMaster-view",
        name: "insurance-master-edit",
        component: InsuranceMasterView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "createInvoice",
        name: "create-invoice-list",
        component: CreateInvoiceList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "createInvoice/createInvoice-add",
        name: "create-invoice-add",
        component: CreateInvoiceAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "createInvoice-bulk",
        name: "invoice-bulk",
        component: CreateInvoiceBulk,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "viewInvoice-bulk",
        name: "view-invoice-bulk",
        component: ViewInvoiceBulk,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "createInvoice/createInvoice-view",
        name: "create-invoice-view",
        component: CreateInvoiceView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "createInvoice/createInvoice-edit",
        name: "create-invoice-edit",
        component: CreateInvoiceEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "createInvoice/createInvoice-copy",
        name: "create-invoice-copy",
        component: CreateInvoiceCopy,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "createInvoice/createInvoice-endorsement",
        name: "create-invoice-endorsement",
        component: CreateInvoiceEndorsement,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "createInvoice/createInvoice-cancellation",
        name: "create-invoice-cancellation",
        component: CreateInvoiceCancellation,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "createInvoice/createInvoice-commission",
        name: "create-invoice-commission",
        component: CreateInvoiceCommission,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "invoiceManagement",
        name: "invoice-management-list",
        component: InvoiceManagementList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "invoiceManagement/invoice-management-view",
        name: "invoice-management-edit",
        component: InvoiceManagementView,
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "invoiceManagement/invoice-management-endor",
        name: "invoice-management-endor",
        component: InvoiceManagementEndor,
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "lapsedInvoice",
        name: "lapsed-invoice-list",
        component: LapsedInvoiceList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "standard-transaction/add",
        name: "standard-transaction-add",
        component: addStandardTransaction,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "standard-transaction/edit",
        name: "standard-transaction-edit",
        component: editStandardTransaction,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "standard-transaction",
        name: "standard-transaction-list",
        component: listStandardTransaction,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },

      {
        path: "createclaims",
        name: "create-claim-view",
        // beforeEnter: ifAuthenticated,
        component: CreateClaimsListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "createclaims/single-insured-list",
        name: "single-insured-list",
        // beforeEnter: ifAuthenticated,
        component: SingleInsuredList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "createclaims/multi-insured-list",
        name: "multi-insured-list",
        // beforeEnter: ifAuthenticated,
        component: MultiInsuredList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "createclaims/create",
        name: "createSingle",
        // beforeEnter: ifAuthenticated,
        component: CreateSingleInsured,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "createclaims/create",
        name: "createMulti",
        // beforeEnter: ifAuthenticated,
        component: CreateMultiInsured,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "queryClaims",
        name: "claim-maintenance-list",
        component: claimMaintenance,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "queryClaims",
        name: "claim-maintenance-edit",
        component: viewMaintenance,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "queryClaims",
        name: "view-claims",
        component: ViewClaimsTrans,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "quotation",
        name: "quotation-list",
        component: QuotationList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "coverNote",
        name: "cover-note-list",
        component: CoverNoteList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "quotation/quotation-edit",
        name: "quotation-edit",
        component: QuotationEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "quotation/quotation-edit2",
        name: "quotation-edit2",
        component: QuotationEditView2,
        // meta: {
        //   requiresAuth: true,
        //   requiresPrivilege: true,
        // },
      },
      {
        path: "placing/view/quotationCreate",
        name: "quotation-add",
        component: QuotationCreate,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "quotation/quotationView/CoverNoteCreate",
        name: "cover-note-add",
        component: CoverNoteCreate,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "coverNote/coverNoteView",
        name: "cover-note-edit",
        component: CoverNoteView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "coverNote/coverNoteView2",
        name: "cover-note-edit2",
        component: CoverNoteView2,
        // meta: {
        //   requiresAuth: true,
        //   requiresPrivilege: true,
        // },
      },
      {
        path: "endorsement",
        name: "endorsement-list",
        component: EndorsementList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "endorsement/endorsementCreate",
        name: "endorsement-add",
        component: EndorsemenCreate,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "endorsement/endorsementView",
        name: "endorsement-edit",
        component: EndorsemenView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "endorsement/endorsementView2",
        name: "endorsement-edit2",
        component: EndorsemenView2,
        // meta: {
        //   requiresAuth: true,
        //   requiresPrivilege: true,
        // },
      },
      {
        path: "placingv2",
        name: "placing-list-v2",
        // beforeEnter: ifAuthenticated,
        component: PlacingListV2,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },

      // finance module
      {
        path: "cash-receipt",
        name: "cash-receipt-list",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "cash-receipt/add",
        name: "cash-receipt-add",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "cash-receipt/edit",
        name: "cash-receipt-edit",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application",
        name: "credit-application-list",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application/add",
        name: "credit-application-add",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application/edit",
        name: "credit-application-edit",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund",
        name: "refund-list",
        // beforeEnter: ifAuthenticated,
        component: RefundListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund/add",
        name: "refund-add",
        // beforeEnter: ifAuthenticated,
        component: RefundAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund/edit",
        name: "refund-edit",
        // beforeEnter: ifAuthenticated,
        component: RefundEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "cash-receipt-manual",
        name: "cash-receipt-manual-list",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptManualListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "cash-receipt-manual/add",
        name: "cash-receipt-manual-add",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptManualAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "cash-receipt-manual/edit",
        name: "cash-receipt-manual-edit",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptManualEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "query-debtor",
        name: "query-debtor-list",
        // beforeEnter: ifAuthenticated,
        component: QueryDebtorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "query-creditor",
        name: "query-creditor-list",
        // beforeEnter: ifAuthenticated,
        component: QueryCreditorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "paid-direct",
        name: "paid-direct-list",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaidDirectList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "paid-direct/add",
        name: "paid-direct-add",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaidDirectAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "paid-direct/edit",
        name: "paid-direct-edit",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaidDirectEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "paid-direct/view",
        name: "paid-direct-view",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaidDirectView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "co-ins-premium-allocation",
        name: "co-ins-premium-allocation-list",
        // beforeEnter: ifAuthenticated,
        component: FinancialPremiumAllocationList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "co-ins-premium-allocation/add",
        name: "co-ins-premium-allocation-add",
        // beforeEnter: ifAuthenticated,
        component: FinancialPremiumAllocationAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "co-ins-premium-allocation/edit",
        name: "co-ins-premium-allocation-edit",
        // beforeEnter: ifAuthenticated,
        component: FinancialPremiumAllocationEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "co-ins-premium-allocation/view",
        name: "co-ins-premium-allocation-view",
        // beforeEnter: ifAuthenticated,
        component: FinancialPremiumAllocationView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-currency",
        name: "journal-currency-list",
        // beforeEnter: ifAuthenticated,
        component: JournalCurrencyList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-currency/add",
        name: "journal-currency-add",
        // beforeEnter: ifAuthenticated,
        component: JournalCurrencyAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-currency/view",
        name: "journal-currency-view",
        // beforeEnter: ifAuthenticated,
        component: JournalCurrencyView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment",
        name: "adjustment-list",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment/add",
        name: "adjustment-add",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment/edit",
        name: "adjustment-edit",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },

      {
        path: "remittance",
        name: "remittance-list",
        // beforeEnter: ifAuthenticated,
        component: FinancialRemittanceList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "remittance/add",
        name: "remittance-add",
        // beforeEnter: ifAuthenticated,
        component: FinancialRemittanceAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "remittance/edit",
        name: "remittance-edit",
        // beforeEnter: ifAuthenticated,
        component: FinancialRemittanceEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "remittance/editable",
        name: "remittance-editable",
        // beforeEnter: ifAuthenticated,
        component: FinancialRemittanceEditable,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "soa-manual",
        name: "soa-manual-list",
        // beforeEnter: ifAuthenticated,
        component: SoaManualListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: true,
        },
      },
      {
        path: "soa-manual/add",
        name: "soa-manual-add",
        // beforeEnter: ifAuthenticated,
        component: SoaManualAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "soa-manual/edit",
        name: "soa-manual-edit",
        // beforeEnter: ifAuthenticated,
        component: SoaManualEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },

      // finance claim module
      {
        path: "query-debtor-claim",
        name: "claims-query-debtor-transaction-list",
        // beforeEnter: ifAuthenticated,
        component: QueryDebtorClaimListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "query-creditor-claim",
        name: "claims-query-creditor-transaction-list",
        // beforeEnter: ifAuthenticated,
        component: QueryCreditorClaimListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "cash-receipt-claim-debtor",
        name: "cash-receipt-claims-debtor-list",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptClaimDebtorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "cash-receipt-claim-debtor/add",
        name: "cash-receipt-claims-debtor-add",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptClaimDebtorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "cash-receipt-claim-debtor/edit",
        name: "cash-receipt-claims-debtor-edit",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptClaimDebtorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "cash-receipt-claim-creditor",
        name: "cash-receipt-claims-creditor-list",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptClaimCreditorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "cash-receipt-claim-creditor/add",
        name: "cash-receipt-claims-creditor-add",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptClaimCreditorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "cash-receipt-claim-creditor/edit",
        name: "cash-receipt-claims-creditor-edit",
        // beforeEnter: ifAuthenticated,
        component: CashReceiptClaimCreditorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "funding-claim-debtor",
        name: "funding-claims-debtor-list",
        // beforeEnter: ifAuthenticated,
        component: FundingClaimDebtorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "funding-claim-debtor/add",
        name: "funding-claims-debtor-add",
        // beforeEnter: ifAuthenticated,
        component: FundingClaimDebtorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "funding-claim-debtor/edit",
        name: "funding-claims-debtor-edit",
        // beforeEnter: ifAuthenticated,
        component: FundingClaimDebtorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application-claim-debtor",
        name: "claims-debtor-credit-application-list",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationClaimDebtorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application-claim-debtor/add",
        name: "claims-debtor-credit-application-add",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationClaimDebtorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application-claim-debtor/edit",
        name: "claims-debtor-credit-application-edit",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationClaimDebtorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment-claim-debtor",
        name: "adjustment-claims-debtor-list",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentDebtorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment-claim-debtor/add",
        name: "adjustment-claims-debtor-add",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentDebtorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment-claim-debtor/edit",
        name: "adjustment-claims-debtor-edit",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentDebtorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund-claim-debtor",
        name: "refund-claims-debtor-list",
        // beforeEnter: ifAuthenticated,
        component: RefundDebtorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund-claim-debtor/add",
        name: "refund-claims-debtor-add",
        // beforeEnter: ifAuthenticated,
        component: RefundDebtorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund-claim-debtor/edit",
        name: "refund-claims-debtor-edit",
        // beforeEnter: ifAuthenticated,
        component: RefundDebtorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment-claim-creditor",
        name: "adjustment-claims-creditor-list",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentCreditorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment-claim-creditor/add",
        name: "adjustment-claims-creditor-add",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentCreditorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "adjustment-claim-creditor/edit",
        name: "adjustment-claims-creditor-edit",
        // beforeEnter: ifAuthenticated,
        component: AdjustmentCreditorrEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund-claim-creditor",
        name: "refund-claims-creditor-list",
        // beforeEnter: ifAuthenticated,
        component: RefundCreditorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund-claim-creditor/add",
        name: "refund-claims-creditor-add",
        // beforeEnter: ifAuthenticated,
        component: RefundCreditorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "refund-claim-creditor/edit",
        name: "refund-claims-creditor-edit",
        // beforeEnter: ifAuthenticated,
        component: RefundCreditorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-claim-debtor",
        name: "claims-debtor-journal-currency-list",
        // beforeEnter: ifAuthenticated,
        component: JournalDebtorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-claim-debtor/add",
        name: "claims-debtor-journal-currency-add",
        // beforeEnter: ifAuthenticated,
        component: JournalDebtorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-claim-debtor/edit",
        name: "claims-debtor-journal-currency-edit",
        // beforeEnter: ifAuthenticated,
        component: JournalDebtorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-claim-creditor",
        name: "claims-creditor-journal-currency-list",
        // beforeEnter: ifAuthenticated,
        component: JournalCreditorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-claim-creditor/add",
        name: "claims-creditor-journal-currency-add",
        // beforeEnter: ifAuthenticated,
        component: JournalCreditorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "journal-claim-creditor/edit",
        name: "claims-creditor-journal-currency-edit",
        // beforeEnter: ifAuthenticated,
        component: JournalCreditorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application-claim-creditor",
        name: "claims-creditor-credit-application-list",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationClaimCreditorListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application-claim-creditor/add",
        name: "claims-creditor-credit-application-add",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationClaimCreditorAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "credit-application-claim-creditor/edit",
        name: "claims-creditor-credit-application-edit",
        // beforeEnter: ifAuthenticated,
        component: CreditApplicationClaimCreditorEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },

      {
        path: "payment-creditor/list",
        name: "claims-creditor-payment-list",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaymentCreditorList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "payment-creditor/add",
        name: "claims-creditor-payment-add",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaymentCreditorAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "payment-creditor/view",
        name: "claims-creditor-payment-view",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaymentCreditorView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "payment-creditor/edit",
        name: "claims-creditor-payment-edit",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaymentCreditorEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },

      {
        path: "payment-debtor/list",
        name: "claims-debtor-payment-list",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaymentDebtorList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "payment-debtor/add",
        name: "claims-debtor-payment-add",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaymentDebtorAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "payment-debtor/edit",
        name: "claims-debtor-payment-edit",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaymentDebtorEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "paid-direct-debtor",
        name: "claims-debtor-paid-direct-list",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaidDirectDebtorList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "paid-direct-debtor/add",
        name: "claims-debtor-paid-direct-add",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaidDirecDebtortAdd,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "paid-direct-debtor/edit",
        name: "claims-debtor-paid-direct-edit",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaidDirectDebtorEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "paid-direct-debtor/view",
        name: "claims-debtor-paid-direct-view",
        // beforeEnter: ifAuthenticated,
        component: FinancialPaidDirectDebtorView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },

      // tax module
      {
        path: "vat-number",
        name: "vat-number-list",
        // beforeEnter: ifAuthenticated,
        component: VatNumberListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "vat-number/add",
        name: "vat-number-add",
        // beforeEnter: ifAuthenticated,
        component: VatNumberAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "vat-number/edit",
        name: "vat-number-edit",
        // beforeEnter: ifAuthenticated,
        component: VatNumberEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "vat-manual",
        name: "vat-req-list",
        // beforeEnter: ifAuthenticated,
        component: VatManualListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "vat-manual/add",
        name: "vat-req-add",
        // beforeEnter: ifAuthenticated,
        component: VatManualAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "vat-manual/edit",
        name: "vat-req-edit",
        // beforeEnter: ifAuthenticated,
        component: VatManualEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "vat-underwriter",
        name: "vat-underwriter-list",
        // beforeEnter: ifAuthenticated,
        component: FakturPajakUnderwriterListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "vat-underwriter/add",
        name: "vat-underwriter-add",
        // beforeEnter: ifAuthenticated,
        component: FakturPajakUnderwriterAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "vat-underwriter/edit",
        name: "vat-underwriter-edit",
        // beforeEnter: ifAuthenticated,
        component: FakturPajakUnderwriterEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "faktur-pajak-lampiran/",
        name: "vat-attachment-view",
        // beforeEnter: ifAuthenticated,
        component: FakturPajakLampiranView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "generate-vat-listing/",
        name: "vat-listing-list",
        // beforeEnter: ifAuthenticated,
        component: GenerateVatListingListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "generate-vat-listing/view",
        name: "generate-vat-listing-view",
        // beforeEnter: ifAuthenticated,
        component: GenerateVatListingView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },

      {
        path: "broking-report",
        name: "broking-report-list",
        // beforeEnter: ifAuthenticated,
        component: BrokingReportList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "finance-report",
        name: "finance-report-list",
        // beforeEnter: ifAuthenticated,
        component: FinanceReportList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "finance-report/view",
        name: "finance-report-view",
        // beforeEnter: ifAuthenticated,
        component: FinanceReportView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "finance-claim-report",
        name: "finance-claim-report-list",
        // beforeEnter: ifAuthenticated,
        component: FinanceClaimReportList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "claim-report",
        name: "claim-report-list",
        // beforeEnter: ifAuthenticated,
        component: ClaimReportList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "it-report",
        name: "it-report-list",
        // beforeEnter: ifAuthenticated,
        component: ITReportList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "accounting-report",
        name: "accounting-report-list",
        // beforeEnter: ifAuthenticated,
        component: AccountingReportList,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "audit-control",
        name: "audit-control-view",
        component: AuditControl,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "closing-control",
        name: "closing-control-view",
        component: ClosingControl,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "budgeting",
        name: "budgeting-list",
        component: Budgeting,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "budgeting/add",
        name: "budgeting-add",
        component: BudgetingAddView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "budgeting/view",
        name: "budgeting-view",
        component: BudgetingEditView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      {
        path: "engineering-fee",
        name: "engineeringfee-list",
        component: EngineeringFeeListView,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
      //EngineeringFeeListEdit
      {
        path: "engineering-fee/edit",
        name: "engineeringfee-edit",
        component: EngineeringFeeListEdit,
        meta: {
          requiresAuth: true,
          requiresPrivilege: false,
        },
      },
    ],
  },
];

const router = new VueRouter({
  routes,
  linkActiveClass: "is-active",
  mode: "history",
});

const vm = new Vue({
  mixins: [authHelper, developmentHelper],
});

router.beforeEach((to, from, next) => {
  // console.log("from:", from, "to:", to);
  let simulate = vm.isDevelopment();
  if (simulate) {
    next();
  } else if (localStorage.getItem("skipLogin")) {
    next();
  } else {
    let privGroup = {
      "currency-manager-list": ["currency", "exchange-rate"],
      "template-configuration-list": [
        "template-config-config",
        "template-config-item",
      ],
    };
    if (to.matched.some((record) => record.meta.requiresAuth)) {
      let token = JSON.parse(localStorage.getItem("token"));
      if (
        token == null ||
        token.access_token == null ||
        token.token_type != "bearer"
      ) {
        next({
          name: "login",
          params: { nextUrl: to.fullPath },
        });
      } else if (
        to.meta.requiresPrivilege &&
        !vm.checkAuthorities(to.name) &&
        privGroup[to.name] != null
      ) {
        // kondisi tambahan untuk menu list yang memiliki tab
        console.log("doesn't have privilege but has group");
        let hasPrivilege = false;
        for (let key of privGroup[to.name]) {
          console.log(key, vm.checkAuthorities(key));
          if (vm.checkAuthorities(key)) {
            hasPrivilege = true;
          }
        }
        if (hasPrivilege) {
          next();
        } else {
          console.log("doesn't have privilege");
          next({
            name: from.name == null ? "home" : from.name,
            params: { nextUrl: to.fullPath },
          });
        }
      } else if (
        to.meta.requiresPrivilege &&
        !vm.checkAuthorities(to.name) &&
        to.name.includes("-edit") &&
        to.query &&
        (to.query.mode == "d" || to.query.statusPage == "view") &&
        vm.checkAuthorities(to.name.replace("-edit", "-list"))
      ) {
        // kondisi tambahan untuk screen detail
        console.log("detail screen privilege");
        next();
      } else if (to.meta.requiresPrivilege && !vm.checkAuthorities(to.name)) {
        console.log("doesn't have privilege");
        next({
          name: from.name == null ? "home" : from.name,
          params: { nextUrl: to.fullPath },
        });
      } else {
        next();
      }
    } else {
      next();
    }
  }
});

export default router;
