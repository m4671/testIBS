// THIS IS SAMPLE BASE REQUEST
// author Austra
import cachedDataHelper from "@/helper/cachedDataHelper";

const axios = require("axios");
const target = process.env.VUE_APP_ENDPOINT;
export default {
  Api(timeout) {
    process.env.VUE_APP_IS_DEVELOPMENT == "true"
      ? console.log(process.env)
      : null;
    let token = JSON.parse(localStorage.getItem("token"));
    if (token == null) {
      // alert("token null");
      window.location.href = "/";
    }
    return axios.create({
      baseURL: target,
      timeout: timeout == null ? 60000 : timeout,
      crossdomain: true,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: token.token_type + " " + token.access_token,
        // 'accept': '*/*',
        // 'Access-Control-Allow-Credentials': 'true',
        // 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, DELETE',
        // 'Access-Control-Allow-Headers': 'Content-Type, Accept, X-Requested-With, Authorization'
      },
    });
  },
  post(url, params, timeout, cancelToken) {
    return new Promise((resolve, reject) => {
      this.Api(timeout)
        .post(url, params, { cancelToken: cancelToken })
        .then(function (response) {
          if (response == null) {
            reject(null);
          } else {
            if (response.status == 200) {
              resolve(response);
            } else {
              resolve(response);
            }
          }
        })
        .catch(function (error) {
          process.env.VUE_APP_IS_DEVELOPMENT == "true"
            ? console.log("Error", error.response)
            : null;
          if (error.response && error.response.status == 401) {
            window.location.href = "/";
          } else if (error.response) {
            reject(new Error(error.response.data.error_description));
          } else if (error.request) {
            reject(new Error("Server cannot be reached"));
          } else if (axios.isCancel) {
            reject(new Error("Request cancelled"));
          } else {
            reject(new Error("Something Wrong"));
          }
        });
    });
  },
  get(url, paramsGet, timeout, cancelToken) {
    let itemsToCached = [
      "master-parameter",
      "master-risk",
      "BUSINESS_CATEGORY",
      "BUSINESS_SEGMENT",
      "BUSINESS_SUBSEGMENT",
      "BANK_RECEIVE_LIST",
      "SYSTEM_FEE_PERCENTAGE",
    ]; //"agent"
    let masterData = JSON.parse(localStorage.getItem("masterData"));
    if (masterData == null) {
      masterData = {};
    }
    let lastUrl = cachedDataHelper.getLastUrl(url);
    return new Promise((resolve, reject) => {
      if (masterData[lastUrl] != null && itemsToCached.indexOf(lastUrl) != -1) {
        let response = cachedDataHelper.getData(
          url,
          lastUrl,
          masterData[lastUrl],
          paramsGet
        );
        resolve(response);
      } else {
        this.Api(timeout)
          .get(url, { params: paramsGet, cancelToken: cancelToken })
          .then(function (response) {
            resolve(response);
            process.env.VUE_APP_IS_DEVELOPMENT == "true"
              ? console.log(lastUrl, itemsToCached.indexOf(lastUrl))
              : null;
            if (itemsToCached.indexOf(lastUrl) != -1) {
              if (lastUrl === "BUSINESS_SUBSEGMENT") {
                let data = response.data.data;
                data.forEach((element) => {
                  element.codeAndName = `${element.code} - ${element.value}`;
                });
                masterData[lastUrl] = data;
              } else {
                masterData[lastUrl] = response.data.data;
              }
              console.log(masterData);
              localStorage.setItem("masterData", JSON.stringify(masterData));
            }
          })
          .catch(function (error) {
            process.env.VUE_APP_IS_DEVELOPMENT == "true"
              ? console.log("Error", error.response)
              : null;
            if (error.response && error.response.status == 401) {
              window.location.href = "/";
            } else if (error.response) {
              reject(new Error(error.response.data.error_description));
            } else if (error.request) {
              reject(new Error("Server cannot be reached"));
            } else if (axios.isCancel) {
              reject(new Error("Request cancelled"));
            } else {
              reject(new Error("Something Wrong"));
            }
          });
      }
    });
  },
};
