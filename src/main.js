import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "@/core/routes";
import store from "./store/store";
import currencyHelper from "@/helper/currencyHelper";
import claimsHelper from "@/helper/claimsHelper";
import authHelper from "@/helper/authHelper";

// Require Froala Editor js file.
require("froala-editor/js/froala_editor.pkgd.min.js");
// Require Froala Editor css files.
require("froala-editor/css/froala_editor.pkgd.min.css");
require("froala-editor/css/froala_style.min.css");
// Import and use Vue Froala lib.
import VueFroala from "vue-froala-wysiwyg";
import VueWysiwyg from "@mycure/vue-wysiwyg";

import { VueMaskDirective } from 'v-mask';

Vue.use(VueFroala, VueWysiwyg);

Vue.directive('mask', VueMaskDirective);

Vue.config.productionTip = false;

new Vue({
  mixins: [currencyHelper, claimsHelper, authHelper],
  vuetify,
  render: (h) => h(App),
  router,
  store,
}).$mount("#app");
