// Mutations
export const SET_SNACKBAR = "snackbar/SET_SNACKBAR";
export const SET_DRAWER = "drawer/SET_DRAWER";

// Actions
export const ACTION_SET_SNACKBAR = "invoice/ACTION_SET_SNACKBAR";
export const ACTION_SET_DRAWER = "drawer/ACTION_SET_DRAWER";
