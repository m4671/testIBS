const developmentHelper = {
    methods: {
        isDevelopment: function () {
            return process.env.VUE_APP_IS_DEVELOPMENT == "true";
        }
    }
}
export default developmentHelper;