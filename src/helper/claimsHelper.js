import Vue from "vue";
const claimsHelper = Vue.mixin({
  methods: {
    getNetReserve: function (list, name = null, paid = 0, collection = []) {
      // Amount loss reserve berkategori CLAIM STATEMENT -
      // Nilai Deductible - salvage/recovery +
      // Payment interim pertama
      let claimStatateRecord = list.filter(
        (value) => value.categoryCode == "1" || value.categoryCode == "0"
      );
      let amount = 0;
      let deductible = 0;
      let recovery = 0;
      let payment = 0;
      if (claimStatateRecord.length > 0) {
        amount = this.currencyToFloat(claimStatateRecord[0].amount);
        deductible = this.currencyToFloat(claimStatateRecord[0].deductible);
        recovery = this.currencyToFloat(claimStatateRecord[0].recovery);
      }
      // let interimRecord = list.filter((value) => ["3", "4"].indexOf(value.categoryCode) != -1);
      // if (interimRecord.length > 0) {
      //   // payment = Math.abs(this.currencyToFloat(interimRecord[0].amount));
      //   for (let interimItem of interimRecord) {
      //     payment = payment + this.currencyToFloat(interimItem.amount);
      //   }
      // }
      payment = paid;
      let result = 0;
      if (name == "amount") {
        result = amount;
      } else if (name == "deductible") {
        result = deductible * -1;
      } else if (name == "recovery") {
        result = recovery * -1;
      } else if (name == "payment") {
        result = payment * -1;
      } else if (name == "effective") {
        result = amount - deductible - recovery;
      } else if (name == "totalCollection") {
        result = collection.reduce(function(sum, current) {
          return sum + current.claimAmount;
        }, 0);
      } else {
        result = amount - deductible - recovery - payment;
      }
      return result;
    },
  },
});
export default claimsHelper;
