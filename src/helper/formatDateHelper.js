const formatDateHelper = {
  formatDateFromYMDToDMY: function (date, splitFrom, splitTo) {
    if (!date) return null;
    const [year, month, day] = date.split(splitFrom);
    return `${day}${splitTo}${month}${splitTo}${year}`;
  },
  formatDateFromDMYToYMD: function (date, splitFrom, splitTo) {
    if (!date) return null;
    const [day, month, year] = date.split(splitFrom);
    return `${year}${splitTo}${month}${splitTo}${day}`;
  },
  formatDateFromDMYoDMY: function (date, splitFrom, splitTo) {
    if (!date) return null;
    const [day, month, year] = date.split(splitFrom);
    return `${day}${splitTo}${month}${splitTo}${year}`;
  },
};

export default formatDateHelper;
