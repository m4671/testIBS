const cachedDataHelper = {
  getLastUrl: function (url) {
    let splittedUrl = url.split("/");
    let splittedLastUrl = splittedUrl[splittedUrl.length - 1].split("?");
    let lastUrl = splittedLastUrl[0];
    if (lastUrl == "") {
      lastUrl = splittedUrl[splittedUrl.length - 2];
    }
    return lastUrl;
  },
  getData: function (url, lastUrl, data, paramsGet) {
    let urlParam = url.split("?")[1];
    let searchParam = new URLSearchParams(urlParam);
    if (paramsGet != null) {
      for (let prop in paramsGet) {
        searchParam.set(prop, paramsGet[prop]);
      }
    }
    // yang sekiranya paramnya dipake: module, sort, isactive?
    if (
      lastUrl == "master-parameter" &&
      searchParam.get("module.equals") != null
    ) {
      data = data.filter(
        (value) => value.module == searchParam.get("module.equals")
      );
    }
    if (
      lastUrl == "master-parameter" &&
      searchParam.get("code.equals") != null
    ) {
      data = data.filter(
        (value) => value.code == searchParam.get("code.equals")
      );
    }
    if (
      lastUrl == "master-parameter" &&
      searchParam.get("code.contains") != null
    ) {
      data = data.filter(
        (value) => value.code.includes(searchParam.get("code.contains"))
      );
    }
    if (
      lastUrl == "master-parameter" &&
      searchParam.get("code.in") != null
    ) {
      let splittedParam = searchParam.get("code.in").split(",");
      let param = [];
      for(var d of splittedParam) {
        param.push(d);
      }
      data = data.filter(
        (value) => param.indexOf(value.code) != -1
      );
    }
    if (searchParam.get("isActive.equals") != null) {
      data = data.filter(
        (value) => value.isActive == searchParam.get("isActive.equals")
      );
    }
    if (searchParam.get("sort") != null) {
      let sort = searchParam.get("sort").split(",");
      data.sort(function (a, b) {
        if (sort[1] == "asc") {
          return a[sort[0]] - b[sort[0]];
        } else if (sort[1] == "desc") {
          return b[sort[0]] - a[sort[0]];
        }
      });
    }

    return { data: { code: 0, data: data } };
  },
};
export default cachedDataHelper;
