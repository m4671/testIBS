var Minio = require("minio");

var minioClient = new Minio.Client({
  endPoint: "mf8.myinfosys.net",
  port: 7005,
  useSSL: false,
  accessKey: "minioadmin",
  secretKey: "minioadmin",
});

const minioHelper = {
  getObjectMinio: function (docUrl, func) {
    minioClient.presignedGetObject("ibs", docUrl, 5 * 60, func);
  },
  putObjectMinio: function (docUrl, func) {
    minioClient.presignedPutObject("ibs", docUrl, 10 * 60, func);
  },
};
export default minioHelper;
