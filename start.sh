set +e
# sudo npm cache clean -f
# sudo npm install -g n
# sudo n stable
# sudo npm install
# sudo rm -rf dist
# sudo npm run build
sudo docker rm -f ibs-portal-dev
sudo docker build -f Dockerfile -t ibs-portal-dev .
sudo docker run -dit --name ibs-portal-dev -p 9099:80 ibs-portal-dev
